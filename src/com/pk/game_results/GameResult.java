package com.pk.game_results;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gem_results")
public class GameResult {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;

	@Column(name = "a_score")
	private String aScore = Score.LOVE.representation;
	@Column(name = "b_score")
	private String bScore = Score.LOVE.representation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAScore() {
		return aScore;
	}

	public void setAScore(String aScore) {
		this.aScore = aScore;
	}

	public String getBScore() {
		return bScore;
	}

	public void setBScore(String bScore) {
		this.bScore = bScore;
	}

	public String getResultString() {
		if (isDeuce()) {
			return "deuce";
		} else {
			return aScore + " " + bScore;
		}
	}

	private boolean isDeuce() {
		return Score.DEUCE.isRepresentedBy(aScore);
	}

	public void notifyAScored() {
		aScore = getNewScoreOfPlayerWhoScored(aScore, bScore);
		bScore = getNewScoreOfPlayerWhoDidntScore(bScore, aScore);
	}
	
	public void notifyBScored() {
		bScore = getNewScoreOfPlayerWhoScored(bScore, aScore);
		aScore = getNewScoreOfPlayerWhoDidntScore(aScore, bScore);
	}

	public void reset() {
		aScore = Score.LOVE.representation;
		bScore = Score.LOVE.representation;
		
	}

	private String getNewScoreOfPlayerWhoScored(String inScore, String outScore) {
		if(Score.WON.isRepresentedBy(outScore)){
			return inScore;
		}
		inScore = Score.getPlusOneScore(inScore);
		if (Score.FORTY.isRepresentedBy(inScore) && inScore.equals(outScore)) {
			return Score.DEUCE.representation;
		} else {
			return inScore;
		}
	}

	private String getNewScoreOfPlayerWhoDidntScore(String outScore,
			String inScore) {
		if (Score.ADVANTAGE.isRepresentedBy(inScore)
				|| Score.WON.isRepresentedBy(inScore)) {
			return Score.LOOSING.representation;
		} else if (Score.DEUCE.isRepresentedBy(inScore)) {
			return Score.DEUCE.representation;
		} else {
			return outScore;
		}
	}

	

}
