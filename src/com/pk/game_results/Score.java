package com.pk.game_results;

import java.util.HashMap;
import java.util.Map;

public enum Score {
	WON("W"), 
	ADVANTAGE("A", WON), 
	FORTY("40", WON), 
	THIRTY("30", FORTY), 
	FIFTEEN("15", THIRTY), 
	LOVE("0", FIFTEEN), 
	DEUCE("D", ADVANTAGE), 
	LOOSING("-", DEUCE);

	public String representation;
	private Score plusOneScore;
	private static Map<String, Score> map = new HashMap<String, Score>();

	static {
		for (Score v : values()) {
			map.put(v.representation, v);
		}
	}

	Score(String s) {
		this(s, null);
	}

	Score(String s, Score plusOneScore) {
		this.representation = s;
		this.plusOneScore = plusOneScore;
	}

	public boolean isRepresentedBy(String s) {
		return representation.equals(s);
	}

	public static String getPlusOneScore(String s) {
		if (WON.isRepresentedBy(s)) {
			return s;
		}
		Score score = map.get(s);
		if (score == null) {
			return "error";
		} else {
			return score.plusOneScore.representation;
		}
	}
}
