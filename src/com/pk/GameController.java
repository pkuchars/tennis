package com.pk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pk.dao.GameDao;
import com.pk.game_results.GameResult;

@Controller
public class GameController {

	@Autowired
	private GameDao gameDao;

	@RequestMapping(value = "/game_result", method = RequestMethod.GET)
	public String getGameResult(ModelMap model) {
		GameResult score = gameDao.getScore();
		model.addAttribute("message", score.getResultString());
		return "game_result";
	}

	@RequestMapping(value = "/player_a_scores", method = RequestMethod.POST)
	public String playerAScores() {
		return new ResultUpdater() {
			@Override
			public void update(GameResult result) {
				result.notifyAScored();
			}
		}.doUpdateAndRedirectToResult();
	}

	@RequestMapping(value = "/player_b_scores", method = RequestMethod.POST)
	public String playerBScores() {
		return new ResultUpdater() {
			@Override
			public void update(GameResult result) {
				result.notifyBScored();
			}
		}.doUpdateAndRedirectToResult();
	}

	@RequestMapping(value = "/reset_score", method = RequestMethod.POST)
	public String resetScore() {
		return new ResultUpdater() {
			@Override
			public void update(GameResult result) {
				result.reset();
			}
		}.doUpdateAndRedirectToResult();
	}

	private abstract class ResultUpdater {
		public String doUpdateAndRedirectToResult() {
			GameResult result = gameDao.getScore();
			update(result);
			gameDao.updateResult(result);
			return "redirect:game_result";
		}

		public abstract void update(GameResult result);
	}

}