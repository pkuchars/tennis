package com.pk.dao;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
//import org.springframework.orm.hibernate3.HibernateTemplate;

import com.pk.game_results.GameResult;

@Transactional
public class GameDaoImpl implements GameDao {
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void updateResult(GameResult score) {
		sessionFactory.getCurrentSession().saveOrUpdate(score);

	}

	@Override
	public GameResult getScore() {
		GameResult r = (GameResult) sessionFactory.getCurrentSession()
				.createQuery("FROM " + GameResult.class.getName())
				.setMaxResults(1).uniqueResult();
		if (r == null) {
			return new GameResult();
		}
		return r;
	}

}
