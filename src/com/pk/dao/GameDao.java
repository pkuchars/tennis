package com.pk.dao;

import com.pk.game_results.GameResult;

public interface GameDao {
	
	public void updateResult(GameResult score);
	public GameResult getScore();
}
