package com.pk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.pk.game_results.GameResult;

public class GameScoreHardFoughtTest {
	
	GameResult score;
	
	@Before
	public void setUp() throws Exception {
		score = new GameResult();
	}

	@Test
	public void deuceTest() {
		

		score.notifyBScored();
		assertEquals("0 15", score.getResultString());
		score.notifyAScored();
		assertEquals("15 15", score.getResultString());
		score.notifyBScored();
		score.notifyBScored();
		assertEquals("15 40", score.getResultString());
		score.notifyAScored();
		assertEquals("30 40", score.getResultString());
		score.notifyAScored();
		assertEquals("deuce", score.getResultString());
		score.notifyBScored();
		assertEquals("- A", score.getResultString());
		score.notifyAScored();
		assertEquals("deuce", score.getResultString());
		score.notifyAScored();
		assertEquals("A -", score.getResultString());
		score.notifyBScored();
		assertEquals("deuce", score.getResultString());
		score.notifyBScored();
		assertEquals("- A", score.getResultString());
		
	}
	
	@Test
	public void winningStabilityTest(){
		score.notifyBScored();
		score.notifyBScored();
		score.notifyBScored();
		
		score.notifyBScored();
		assertEquals("- W", score.getResultString());
		
		score.notifyBScored();
		assertEquals("- W", score.getResultString());
		
		score.notifyAScored();
		assertEquals("- W", score.getResultString());
	}

}
