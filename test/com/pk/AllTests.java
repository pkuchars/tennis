package com.pk;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ GameScoreAWinsTest.class, GameScoreBWinsTest.class,
		GameScoreHardFoughtTest.class })
public class AllTests {

}
