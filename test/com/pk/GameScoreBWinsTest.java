package com.pk;

import static org.junit.Assert.*;

import org.junit.Test;

import com.pk.game_results.GameResult;

public class GameScoreBWinsTest {

	@Test
	public void test() {
		GameResult score = new GameResult();

		assertEquals("0 0", score.getResultString());
		score.notifyBScored();
		assertEquals("0 15", score.getResultString());
		score.notifyBScored();
		assertEquals("0 30", score.getResultString());
		score.notifyBScored();
		assertEquals("0 40", score.getResultString());
		score.notifyBScored();
		assertEquals("- W", score.getResultString());
	}

}
