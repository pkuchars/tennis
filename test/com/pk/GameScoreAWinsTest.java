package com.pk;

import static org.junit.Assert.*;

import org.junit.Test;

import com.pk.game_results.GameResult;

public class GameScoreAWinsTest {

	@Test
	public void test() {
		GameResult score = new GameResult();
		
		assertEquals("0 0",score.getResultString());
		score.notifyAScored();
		assertEquals("15 0",score.getResultString());
		score.notifyAScored();
		assertEquals("30 0",score.getResultString());
		score.notifyAScored();
		assertEquals("40 0",score.getResultString());
		score.notifyAScored();
		assertEquals("W -",score.getResultString());
		
	}

}
