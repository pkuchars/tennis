<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Game result</title>
</head>
<body>
	<h2>${message}</h2>
	<form name="a_scores" action="player_a_scores" method="POST">
		<input type="submit" value="Player A scores"><br>
	</form>
	<form name="b_scores" action="player_b_scores" method="POST">
		<input type="submit" value="Player B scores"><br>
	</form>
	<form name="reset_score" action="reset_score" method="POST">
		<input type="submit" value="Reset score"><br>
	</form>

</body>
</html>